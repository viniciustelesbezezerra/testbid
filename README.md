oding Challenge
====================

Considerações Gerais
====================
Você deverá usar o repositório que lhe passei como o repo principal do projeto, i.e., todos os seus commits devem estar registrados nele, pois queremos ver como você trabalha.

Esse problema tem 2 constraints :

a) Eu preciso conseguir rodar seu código no mac os x OU no ubuntu;

b) Eu executarei seu código com os seguintes comandos ou algo similar ( dê-me as instruções ) :

    git clone seu-fork
    cd seu-fork
    ./configure
    make

Esses comandos tem que ser o suficiente para configurar meu mac os x OU ubuntu e rodar seu programa.

Pode considerar que eu tenho instalado no meu sistema Python. Qualquer outra dependência que eu precisar vc tem que prover.

O Problema
====================

O problema que você tem que resolver é Desenvolver um encurtador de URL em versão WEB com HTML/CSS/Javascript + o backend usando Python/Django.


Desenvolva uma aplicação web para encurtar urls (como o http://goo.gl/). A URL encurtada deve ser composta de letras e números apenas.

Alguns casos de aceite:

A.1 Cadastro e autenticação simples de usuário.

A.2 Usuário ou anônimo cadastra uma url e tem como retorno a url encurtada:

Exemplo:

Usuário cadastra a url http://brbid.com/empresas e o encurtador retorna uma url encurtada.

A.3 Usuário, ao se logar, verá todas as urls que ele cadastrou.

A.4 Ao acessar a url encurtada, o usuário deve ser redirecionado para a url original (no exemplo citado, a url original seria http://brbid.com/empresas)

Registre tudo : testes que forem executados, idéias que gostaria de implementar se tivesse tempo (explique como você as resolveria, se houvesse tempo), decisões que forem tomadas e seus por quês, arquiteturas que forem testadas e os motivos de terem sido modificadas ou abandonadas.


O que será avaliado na sua solução ?
====================================
1. No seu código será avaliado a simplicidade e clareza da solução, a arquitetura, documentação, estilo de código, testes unitários, testes funcionais, nível de automação dos testes, o design da interface e a implementação do código.


=====================================

Done it

Just run

$ ./configure
$ ./start

And you are ready to go

=====================================
