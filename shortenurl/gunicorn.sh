#!/bin/bash

set -e

LOGFILE=/home/ubuntu/virtualenvs/test/hello/hello.log

ERRORFILE=/home/ubuntu/virtualenvs/test/hello/error.log

LOGDIR=$(dirname $LOGFILE)

NUM_WORKERS=4

#The below address:port info will be used later to configure Nginx with Gunicorn

ADDRESS=127.0.0.1:8000

# user/group to run as

#USER=your_unix_user

#GROUP=your_unix_group

#cd /home/ubuntu/virtualenvs/testbid/hello/
cd /home/ubuntu/virtualenvs/testbid/shortenurl/

source ../bin/activate

testbid -d $LOGDIR || mkdir -p $LOGDIR

#export SCRIPT_NAME='/bid/'

#exec ../bin/gunicorn_django -w $NUM_WORKERS --bind=$ADDRESS \
exec gunicorn shortenurl.wsgi:application DJANGO_SETTINGS_MODULE=shortenurl.settings
    -w $NUM_WORKERS --bind=$ADDRESS \
    
    --log-level=debug \

    --log-file=$LOGFILE 2>>$LOGFILE  1>>$ERRORFILE  &

