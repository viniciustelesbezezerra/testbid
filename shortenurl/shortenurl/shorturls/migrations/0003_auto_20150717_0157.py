# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cuser.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('shorturls', '0002_auto_20150712_0442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shorturl',
            name='user',
            field=cuser.fields.CurrentUserField(null=True, editable=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
