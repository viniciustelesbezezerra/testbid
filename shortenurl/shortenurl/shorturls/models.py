from django.db import models
from django.contrib.auth.models import User
from pyshorteners.shorteners  import Shortener
from django.core.urlresolvers import reverse
from cuser import middleware, fields


class OwnerManager(models.Manager):

    def get_queryset(self):
        current_user = middleware.CuserMiddleware.get_user()

        if current_user.is_anonymous():
            current_user = None

        return super(OwnerManager, self).get_queryset() \
            .filter(user=current_user)


class ShortUrl(models.Model):
    user = fields.CurrentUserField() #ForeignKey(User)
    original = models.URLField()
    short = models.URLField()

    objects = models.Manager()
    owner_objects = OwnerManager()

    def __unicode__(self):
        return self.original

    def get_absolute_url(self):
        return reverse('shorturl_detail', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):
        self.__set_short()
        super(ShortUrl, self).save(*args, **kwargs)

    def __set_short(self):
        shortener = Shortener('TinyurlShortener')
        self.short = shortener.short(self.original)
