from .support_test_views import UserSupport
from django.core.urlresolvers import reverse
from .test_models import ShortUrlTest
from django.contrib.auth import get_user_model


class SupportViewsTest(ShortUrlTest):

    def create_shorurl_with_logged_user(self):
        self.login_user()

        self.make_object(
            url='http://www.google.com',
            user=get_user_model().objects.first()
        )


class HomePageTest(SupportViewsTest):

    def test_home_page_not_signed_in(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 302)

    def test_home_page_signed_in(self):
        self.create_shorurl_with_logged_user()

        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertIn('Add',
            response.rendered_content)
        self.assertIn('http://tinyurl.com/1c2',
            response.rendered_content)
        self.assertTemplateUsed(response,
            'shorturls/shorturl_list.html')


class CreateShortUrlTest(UserSupport):

    def test_get_create_url_signed_in(self):
        self.login_user()

        response = self.client.get(reverse('shorturl_add'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
            'shorturls/shorturl_form.html')

    def test_post_create_url_signed_in(self):
        self.login_user()

        response = self.client.post(
            reverse('shorturl_add'),
            {'original': 'http://google.com'}
        )
        self.assertEqual(response.status_code, 302)


class DetailShortUrlTest(SupportViewsTest):

    def test_get_detail_url_signed_in(self):
        self.create_shorurl_with_logged_user()

        response = self.client.get(
            reverse('shorturl_detail',
                args=(self.short_url.id,))
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('http://www.google.com',
            response.rendered_content)
        self.assertTemplateUsed(response,
            'shorturls/shorturl_detail.html')


class UpdateShortUrlTest(SupportViewsTest):

    def test_get_update_url_not_signed_in(self):
        response = self.client.get(
            reverse('shorturl_update', args=(1,))
        )
        self.assertEqual(response.status_code, 302)

    def test_get_update_url_signed_in(self):
        self.create_shorurl_with_logged_user()

        response = self.client.get(
            reverse('shorturl_update',
                args=(self.short_url.id,))
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
            'shorturls/shorturl_form.html')

    def test_post_update_url_signed_in(self):
        self.create_shorurl_with_logged_user()

        response = self.client.post(
            reverse('shorturl_update',
                args=(self.short_url.id,)),
            {'original': 'http://google.com'}
        )
        self.assertEqual(response.status_code, 302)


class DeleteShortUrlTest(SupportViewsTest):

    def test_get_delete_url_not_signed_in(self):
        response = self.client.get(
            reverse('shorturl_delete', args=(1,))
        )
        self.assertEqual(response.status_code, 302)

    def test_get_delete_url_signed_in(self):
        self.create_shorurl_with_logged_user()

        response = self.client.get(
            reverse('shorturl_delete',
                args=(self.short_url.id,))
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
            'shorturls/shorturl_confirm_delete.html')

    def test_post_delete_url_signed_in(self):
        self.create_shorurl_with_logged_user()

        response = self.client.post(
            reverse('shorturl_delete',
                args=(self.short_url.id,)),
            {'original': 'http://google.com'}
        )
        self.assertEqual(response.status_code, 302)
