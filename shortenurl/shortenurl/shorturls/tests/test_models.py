from .support_test_views import UserSupport
from ..models import ShortUrl


class ShortUrlTest(UserSupport):

    def make_object(self, **params):
        self.short_url = ShortUrl.objects \
            .create(
                original=params.get('url', None),
                user=params.get('user', None)
            )


class ShortUrlWithUserTest(ShortUrlTest):

    def setUp(self):
        super(ShortUrlWithUserTest, self) \
            .make_object(
                user=self.create_user(),
                url='http://www.google.com'
            )

    def test_values_with_user(self):
        self.assertEqual(self.short_url.original, 'http://www.google.com')
        self.assertEqual(self.short_url.short, 'http://tinyurl.com/1c2')
        self.assertEqual(self.short_url.user.username, 'teste')
        self.assertEqual(self.short_url.__unicode__(), 'http://www.google.com')
        self.assertEqual(self.short_url.get_absolute_url(), '/shorturl/detail/1/')
