from django.test import TestCase
from django.contrib.auth import get_user_model


class UserSupport(TestCase):

    def create_user(self):
        return get_user_model().objects \
            .create_user('teste', 'teste@teste.tld', 'senha123')

    def login_user(self):
        self.create_user()
        self.client.login(username='teste', password='senha123')
