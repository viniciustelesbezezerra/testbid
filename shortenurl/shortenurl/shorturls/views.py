from django.views import generic
from braces import views as braces
from django.core.urlresolvers import reverse_lazy
from .models import ShortUrl


class QuerySetMixin:

    def get_queryset(self):
        return ShortUrl.owner_objects.all()


class ModelFieldMixin:
    model = ShortUrl
    fields = ['original']


class IndexView(braces.LoginRequiredMixin,
    QuerySetMixin, generic.ListView):
    pass


class ShortUrlCreate(ModelFieldMixin, generic.CreateView):
    pass


class ShortUrlDetail(QuerySetMixin, generic.DetailView):
    pass


class ShortUrlUpdate(braces.LoginRequiredMixin,
    QuerySetMixin, ModelFieldMixin, generic.UpdateView):
    pass


class ShortUrlDelete(braces.LoginRequiredMixin,
    ModelFieldMixin, generic.DeleteView, QuerySetMixin):
    success_url = reverse_lazy('home')
