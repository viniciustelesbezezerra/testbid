from django.conf.urls import include, url
from django.contrib import admin
from .shorturls import views


urlpatterns = [
    # url(r'^$', 'shortenurl.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^bid/login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^bid/logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^bid/accounts/', include('registration.backends.simple.urls')),

    url(r'^$', views.IndexView.as_view(), name='home'),

    url(r'^bid/$', views.IndexView.as_view(), name='homebid'),
    
    url(r'^bid/shorturl/add/$', views.ShortUrlCreate.as_view(),
        name='shorturl_add'),
    url(r'^bid/shorturl/detail/(?P<pk>[0-9]+)/$', views.ShortUrlDetail.as_view(),        name='shorturl_detail'),
    url(r'^bid/shorturl/(?P<pk>[0-9]+)/$', views.ShortUrlUpdate.as_view(),
        name='shorturl_update'),
    url(r'^bid/shorturl/(?P<pk>[0-9]+)/delete/$', views.ShortUrlDelete.as_view(),
        name='shorturl_delete'),

    url(r'^bid/admin/', include(admin.site.urls)),
]

